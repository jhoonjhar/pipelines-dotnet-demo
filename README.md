# Pipelines .NET Core Demo
A straight forward implementation of Bitbucket's Pipelines with a .NET Core project.

In this project, I test a very simple .NET Core class library.

## Components

1. Source - [MathService.cs](https://bitbucket.org/liammoat/pipelines-dotnet-demo/src/master/src/MathDemo/MathService.cs)
2. Tests - [MathServiceTests.cs](https://bitbucket.org/liammoat/pipelines-dotnet-demo/src/master/test/MathDemo.Tests/MathServiceTests.cs)
3. Pipelines config - [bitbucket-pipelines.yml](https://bitbucket.org/liammoat/pipelines-dotnet-demo/src/master/bitbucket-pipelines.yml) 

### bitbucket-pipelines.yml

```
#!yaml
image: microsoft/dotnet:onbuild

pipelines:
  default:
    - step:
        script:
          - dotnet restore
          - dotnet test test/MathDemo.Tests
```

[![Pipelines](https://img.shields.io/badge/Bitbucket-Pipelines-blue.svg?style=flat)](https://bitbucket.org/liammoat/pipelines-dotnet-demo/addon/pipelines/home#!/) 

## Todo

1. Publish NuGet Package

## License
Copyright (c) 2015 Liam Moat

Released under [the MIT license](https://bitbucket.org/liammoat/pipelines-dotnet-demo/raw/master/LICENCE).